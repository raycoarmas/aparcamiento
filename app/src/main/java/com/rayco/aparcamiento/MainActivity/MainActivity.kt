package com.rayco.aparcamiento.MainActivity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter

import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.rayco.aparcamiento.AddCarActivity.AddCarActivity
import com.rayco.aparcamiento.Constants.Constants
import com.rayco.aparcamiento.Database.AppDatabase
import com.rayco.aparcamiento.Entity.Car
import com.rayco.aparcamiento.R
import kotlinx.android.synthetic.main.activity_main.*
import com.rayco.aparcamiento.dialogs.Dialogs

import java.util.ArrayList
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    private var permissionResume = false
    private var car : Car? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), Constants.PERMISSION_ACCESS_FINE_LOCATION)
            permissionResume = true
        }
        setContentView(R.layout.activity_main)
        parkOnClickListener()
        findCarOnClickListener()
        addCarOnClickListener()
        spinnerChangeListener()

        changeButtonStatus(false)

    }

    private fun changeButtonStatus(status : Boolean) {
        park.isActivated = status
        park.isEnabled = status
        findCar.isActivated = status
        findCar.isEnabled = status
    }

    override fun onResume() {
        super.onResume()
        if (!permissionResume) {
            loadSpinner()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        permissionResume = false
        when (requestCode) {
            Constants.PERMISSION_ACCESS_FINE_LOCATION -> {
                if (grantResults.isEmpty() || grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    park.isActivated = false
                    park.isEnabled = false
                }
            }
        }
    }

    private var handler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            if (msg.what == Constants.CAR_LIST) {
                val cars = msg.obj as List<Car>
                val carsString = ArrayList<String?>()
                carsString.add("")
                cars.forEach { car ->
                    var carString: String? = if (car.carName != null && car.carName!!.isNotEmpty()) getString(R.string.carName) + ": " + car.carName else ""
                    if (carString != null && carString.isNotEmpty()) {
                        carString += if (car.carPlate != null && car.carPlate!!.isNotEmpty()) " " + getString(R.string.plate) + ": " + car.carPlate else ""
                    } else {
                        carString = if (car.carPlate != null && car.carPlate!!.isNotEmpty()) getString(R.string.plate) + ": " + car.carPlate else ""
                    }
                    carsString.add(carString)
                }

                val arrayAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, carsString)
                carSpinner.adapter = arrayAdapter
            } else if (msg.what == Constants.CHANGE_BUTTON_STATUS){
                changeButtonStatus(car != null )
            }else{
                dialog(msg.what, msg.obj as Int)
            }
        }
    }

    private fun dialog(dialog: Int, descriptionId: Int?) {
        Dialogs.dialog(dialog, descriptionId, this)
    }


    private fun parkOnClickListener() {
        park.setOnClickListener { _ ->

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return@setOnClickListener
            }
            val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
            val locationRequest = LocationRequest()
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, MyLocationListener(applicationContext, car), null)
        }
    }

    private fun findCarOnClickListener() {
        findCar.setOnClickListener { _ ->
            thread(true, block = {
                val database = AppDatabase.getInstance(this)
                val parking = database?.parkingDAO()?.findParkingByCar(car?.idCar)
                AppDatabase.destroyInstance()
                if (parking == null) {
                    val msg = Message()
                    msg.what = Constants.EMPTY_PARKING
                    msg.obj = R.string.description_empty_parking
                    handler.sendMessage(msg)
                    return@thread

                }
                val gmmIntentUri = Uri.parse("google.navigation:q=" + parking.latitude + "," + parking.longitude + "&mode=w&avoid=thf")
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.`package` = "com.google.android.apps.maps"
                startActivity(mapIntent)
            })
        }
    }

    private fun addCarOnClickListener() {

        addCar.setOnClickListener { _ ->
            val addCarIntent = Intent(applicationContext, AddCarActivity::class.java)
            startActivity(addCarIntent)
        }
    }

    private fun spinnerChangeListener() {
        carSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                thread(true, block = {
                    val database = AppDatabase.getInstance(applicationContext)
                    val carString =carSpinner.selectedItem.toString()
                    var tradedCarString = carString.replace(getString(R.string.carName)+": ", ";")
                    tradedCarString = tradedCarString.replace(getString(R.string.plate)+": ",";")
                    tradedCarString = tradedCarString.replaceFirst(";","")
                    val carSplitString = tradedCarString.split(";")
                    val carName = if (carString.contains(getString(R.string.carName))){
                        carSplitString[0].trim()
                    }else {
                        ""
                    }
                    val carPlate = if (carString.contains(getString(R.string.plate)) && carSplitString.size == 1){
                          carSplitString[0].trim()
                    }else if (carString.contains(getString(R.string.plate)) && carSplitString.size == 2) {
                          carSplitString[1].trim()
                    }else {
                        ""
                    }
                    car = database?.carDAO()?.findCarByNamePlate(carName,carPlate)
                    val msg = Message()
                    msg.what = Constants.CHANGE_BUTTON_STATUS
                    handler.sendMessage(msg)

                })
            }
        }
    }

    private fun loadSpinner() {
        thread(true, block = {
            val database = AppDatabase.getInstance(this)
            val cars = database?.carDAO()?.findAllCar()
            AppDatabase.destroyInstance()
            if (cars != null) {
                if (cars.isEmpty()) {
                    val msg = Message()
                    msg.what = Constants.EMPTY_CAR
                    msg.obj = R.string.description_empty_car
                    handler.sendMessage(msg)
                    return@thread
                }
            }
            val msg = Message()
            msg.what = Constants.CAR_LIST
            msg.obj = cars
            handler.sendMessage(msg)
        })

    }

}
