package com.rayco.aparcamiento.MainActivity

import android.content.Context

import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.rayco.aparcamiento.Database.AppDatabase
import com.rayco.aparcamiento.Entity.Car
import com.rayco.aparcamiento.Entity.Parking
import kotlin.concurrent.thread

class MyLocationListener internal constructor(private val context: Context, val car: Car?) : LocationCallback() {

    override fun onLocationResult(locationResult: LocationResult?) {
        thread(true, block = {
            val database = AppDatabase.getInstance(context)
            var parking = database?.parkingDAO()?.findParkingByCar(car?.idCar)
            if (parking == null) {
                parking = Parking()
            }
            parking.latitude = locationResult!!.lastLocation.latitude
            parking.longitude = locationResult.lastLocation.longitude
            parking.carid = car?.idCar
            if (parking.idParking == null) {
                database?.parkingDAO()?.insert(parking)
            } else {
                database?.parkingDAO()?.update(parking)
            }
            AppDatabase.destroyInstance()
        })
    }
}
