package com.rayco.aparcamiento.DAO

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

import com.rayco.aparcamiento.Entity.Parking

@Dao
interface ParkingDAO {

    @Query("Select * from Parking")
    fun findAllParking(): List<Parking>

    @Insert
    fun insert(parking: Parking)

    @Update
    fun update(parking: Parking)

    @Query("select * from Parking where car_id = :carId")
    fun findParkingByCar(carId: Long?): Parking
}
