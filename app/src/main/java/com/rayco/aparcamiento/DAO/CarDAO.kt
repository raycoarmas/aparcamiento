package com.rayco.aparcamiento.DAO

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

import com.rayco.aparcamiento.Entity.Car

@Dao
interface CarDAO {

    @Query("select * from Car")
    fun findAllCar(): List<Car>

    @Insert
    fun insert(car: Car)

    @Update
    fun update(car: Car)

    @Query("select * from Car where carName = :carName or carPlate = :carPlate")
    fun findCarByNamePlate(carName : String, carPlate : String): Car
}
