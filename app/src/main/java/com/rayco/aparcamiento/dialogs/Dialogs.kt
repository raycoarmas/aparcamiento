package com.rayco.aparcamiento.dialogs

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AlertDialog

import com.rayco.aparcamiento.AddCarActivity.AddCarActivity
import com.rayco.aparcamiento.Constants.Constants
import com.rayco.aparcamiento.R

object Dialogs {

    fun dialog(dialogConstant: Int, descriptionId: Int?, activity: Activity) {

        val alertDialogBuilder = AlertDialog.Builder(activity)
        alertDialogBuilder
                .setMessage(activity.getString(descriptionId!!))
                .setCancelable(false)
        if (dialogConstant != Constants.EMPTY_CAR) {
            alertDialogBuilder.setNegativeButton(R.string.cancel
            ) { dialog, _ -> dialog.cancel() }
        }
        when (dialogConstant) {
            Constants.ACTIVATE_GPS -> {
                alertDialogBuilder.setPositiveButton(R.string.open_configuration
                ) { dialog, _ ->
                    val callGPSSettingIntent = Intent(
                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    activity.startActivity(callGPSSettingIntent)
                    dialog.cancel()
                }
                alertDialogBuilder.setPositiveButton(R.string.accept
                ) { dialog, _ ->
                    dialog.cancel()
                    val addCarIntent = Intent(activity.applicationContext, AddCarActivity::class.java)
                    addCarIntent.action = Intent.ACTION_MAIN
                    activity.startActivity(addCarIntent)
                }
            }
            Constants.EMPTY_CAR -> alertDialogBuilder.setPositiveButton(R.string.accept) { dialog, _ ->
                dialog.cancel()
                val addCarIntent = Intent(activity.applicationContext, AddCarActivity::class.java)
                addCarIntent.action = Intent.ACTION_MAIN
                activity.startActivity(addCarIntent)
            }
        }
        val alert = alertDialogBuilder.create()
        alert.show()
    }
}
