package com.rayco.aparcamiento.Database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

import com.rayco.aparcamiento.DAO.CarDAO
import com.rayco.aparcamiento.DAO.ParkingDAO
import com.rayco.aparcamiento.Entity.Car
import com.rayco.aparcamiento.Entity.Parking

@Database(entities = [(Parking::class), (Car::class)], version = 1,exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun parkingDAO(): ParkingDAO
    abstract fun carDAO(): CarDAO

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            AppDatabase::class.java, "parkingAPP.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}

