package com.rayco.aparcamiento.Constants

object Constants {

    const val PERMISSION_ACCESS_FINE_LOCATION = 1
    const val ACTIVATE_GPS = 2
    const val EMPTY_PARKING = 3
    const val SAVE_CAR = 4
    const val EMPTY_CAR = 5
    const val CAR_LIST = 6
    const val CHANGE_BUTTON_STATUS = 7
}
