package com.rayco.aparcamiento.Entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

import android.arch.persistence.room.ForeignKey.CASCADE

@Entity(tableName = "Parking", foreignKeys = [(ForeignKey(entity = Car::class,
                                                          parentColumns = arrayOf("idCar"),
                                                          childColumns = arrayOf("car_id"),
                                                          onDelete = CASCADE,
                                                          onUpdate = CASCADE))])
class Parking {
    @PrimaryKey
    var idParking: Long? = null

    @ColumnInfo(name = "latitude")
    var latitude: Double? = null

    @ColumnInfo(name = "longitude")
    var longitude: Double? = null

    @ColumnInfo(name = "car_id", index = true)
    var carid: Long? = null
}
