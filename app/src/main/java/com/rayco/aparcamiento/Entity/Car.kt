package com.rayco.aparcamiento.Entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

import android.arch.persistence.room.ForeignKey.CASCADE

@Entity(tableName = "Car", indices = [(Index(value = arrayOf("carPlate", "carName"), unique = true))])
class Car {

    @PrimaryKey
    var idCar: Long? = null

    @ColumnInfo(name = "carPlate")
    var carPlate: String? = null

    @ColumnInfo(name = "carName")
    var carName: String? = null

}
