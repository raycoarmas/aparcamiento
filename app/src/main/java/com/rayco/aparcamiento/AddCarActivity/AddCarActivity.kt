package com.rayco.aparcamiento.AddCarActivity

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.rayco.aparcamiento.Constants.Constants
import com.rayco.aparcamiento.Database.AppDatabase
import com.rayco.aparcamiento.Entity.Car
import com.rayco.aparcamiento.R
import com.rayco.aparcamiento.dialogs.Dialogs
import kotlin.concurrent.thread
import kotlinx.android.synthetic.main.activity_add_car.*

class AddCarActivity : AppCompatActivity() {
   
    private var dialogHandler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            dialog(msg.what, msg.obj as Int)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_car)
        saveCarOnClickListener()
        salir.setOnClickListener { _ -> onBackPressed() }

    }

    private fun saveCarOnClickListener() {
        saveCar.setOnClickListener { _ ->
            val plateString = plate.text.toString()
            val nameString = name.text.toString()
            if (plateString.isNotEmpty() || nameString.isNotEmpty()) {
                saveCar(plateString, nameString)
            }
            plate.setText("")
            name.setText("")
        }
    }

    private fun saveCar(plateString: String, nameString: String) {
        thread(true, block = {
            val database = AppDatabase.getInstance(this)
            val car = Car()
            car.carPlate = plateString
            car.carName = nameString
            database?.carDAO()?.insert(car)
            AppDatabase.destroyInstance()
            val msg = Message()
            msg.what = Constants.SAVE_CAR
            msg.obj = R.string.description_save_car
            dialogHandler.sendMessage(msg)
        })
    }

    private fun dialog(dialog: Int, descriptionId: Int?) {
        Dialogs.dialog(dialog, descriptionId, this)
    }
}
